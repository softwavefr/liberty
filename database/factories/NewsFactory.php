<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use Faker\Generator as Faker;

$factory->define(App\Models\News::class, function (Faker $faker) {

    return [
        'title' => $faker->sentence($nbWords = 5, $variableNbWords = true),
        'texte' => $faker->paragraph($nbSentences = 50, $variableNbSentences = true),
        'category' => $faker->randomElement($array = array ('news', 'coverage')),
        'active' => rand(0, 1),
    ];
});
