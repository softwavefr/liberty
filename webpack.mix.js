const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/vue.js', 'public/js');
mix.js('resources/js/map-category.js', 'public/js');
mix.js('resources/js/map-detail.js', 'public/js');


mix.js('resources/js/sb-admin.js', 'public/js')
    .sass('resources/sass/sb-admin/sb-admin.scss', 'public/css');

mix.js('resources/js/theme.js', 'public/js')
    .sass('resources/sass/front/app.scss', 'public/css');

mix.js('resources/js/dropzone.js', 'public/js')
    .sass('resources/sass/dropzone.scss', 'public/css');

mix.js('resources/js/card.js', 'public/js')
    .sass('resources/sass/card.scss', 'public/css');


