<?php
namespace App\Repositories;

use App\Models\News;
use Carbon\Carbon;

class NewsRepository
{
    /**
     * Search.
     *
     * @param \Illuminate\Http\Request $request
     */
    public function search($request)
    {
        /*
        $ads = Ad::query();
        if($request->region != 0) {
            $ads = Ad::whereHas('region', function ($query) use ($request) {
                $query->where('regions.id', $request->region);
            })->when($request->departement != 0, function ($query) use ($request) {
                return $query->where('departement', $request->departement);
            })->when($request->commune != 0, function ($query) use ($request) {
                return $query->where('commune', $request->commune);
            });
        }
        if($request->category != 0) {
            $ads->whereHas('category', function ($query) use ($request) {
                $query->where('categories.id', $request->category);
            });
        }
        return $ads->with('category', 'photos')
            ->whereActive(true)
            ->latest()
            ->paginate(3);
        */
    }

    /**
     * Last.
     *
     * @param \Illuminate\Http\Request $request
     */
    public function Last($num=10, $cate = 'news')
    {
        $news = News::query();

        return $news
            ->with('photos')
            ->whereActive(true)
            ->where('category', $cate)
            ->orderBy('created_at', 'desc')
            ->paginate($num);
    }




    public function getPhotos($id)
    {
        return $id->photos()->get();
    }

    public function getById($id)
    {
        return News::findOrFail($id);
    }

    public function create($data)
    {
        return News::create($data);
    }


    public function update($news, $data)
    {

        $news->update($data);
    }


    public function delete($ad)
    {
        $ad->delete();
    }

    public function obsolete($nbr)
    {
        return Ad::where('limit', '<', Carbon::now())->latest('limit')->paginate($nbr);
    }

    public function activeCount($ads)
    {
        return $ads->where('active', true)->where('limit', '>=', Carbon::now())->count();
    }
    public function getByUser($user)
    {
        return $user->ads()->get();
    }

    public function active($user, $nbr)
    {
        return $user->ads()->whereActive(true)->where('limit', '>=', Carbon::now())->paginate($nbr);
    }



    public function attente($user, $nbr)
    {
        return $user->ads()->whereActive(false)->paginate($nbr);
    }

    public function obsoleteForUser($user, $nbr)
    {
        return $user->ads()->where('limit', '<', Carbon::now())->latest('limit')->paginate($nbr);
    }
}
