<?php

namespace App\Repositories;

use App\Models\Parameter;


class ParameterRepository
{

    /**
     * Search.
     *
     * @param \Illuminate\Http\Request $request
     */
    public function getParam($group, $title)
    {
        $param = Parameter::query();

        return $param
            ->from('parameter')
            ->where('group', $group)
            ->where('title', $title)
            ->orderByDesc('filter');

    }
}
