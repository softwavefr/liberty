<?php
namespace App\Repositories;

use App\Models\Chat;
use App\Models\News;
use Carbon\Carbon;
use Mockery\Generator\Parameter;

class ChatRepository
{
    /**
     * Search.
     *
     * @param \Illuminate\Http\Request $request
     */
    public function search($from_user, $to_user, $limit=10)
    {

        $chat = Chat::query();

        $data =  $chat->join('users', 'users.id', '=', 'chats.from_user')
            ->where(function ($query) use ($from_user, $to_user) {
                $query->where('from_user', '=', $from_user)
                ->where('to_user', '=', $to_user);

            })
            ->orwhere(function ($query) use ($from_user, $to_user) {
                $query->where('from_user', '=', $to_user)
                    ->where('to_user', '=', $from_user);
            })
            ->orderBy('chats.created_at', 'desc')
            ->paginate($limit)->toArray();

        return $data['data'];
    }

    /**
     * Search.
     *
     * @param \Illuminate\Http\Request $request
     */
    public function listWriter($to_user, $limit=10)
    {

        $chat = Chat::query();

        $data =  $chat
            ->select('from_user', 'name')
            ->join('users', 'users.id', '=', 'chats.from_user')
            ->where(function ($query) use ($to_user) {
                $query->where('to_user', '=', $to_user);

            })->distinct()
            ->orderBy('chats.created_at', 'asc')
            ->paginate($limit)
            ->toArray();

        return $data['data'];
    }

}
