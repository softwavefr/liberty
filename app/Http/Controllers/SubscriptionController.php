<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\User;
use Stripe;
use Session;
use Exception;

class SubscriptionController extends Controller
{

    public function index()
    {

        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
        $plane = \Stripe\Plan::all();

        return view('subscription.create',
            compact('plane')
        );
    }



    public function show()
    {

        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));


        $prods = \Stripe\Product::all(['active' => true], ['sort' => 'order_by']);
        $plane = \Stripe\Plan::all();

        $arr = $plane['data'];
        usort($arr, function($a, $b) {
            return $a['amount'] <=> $b['amount'];
        });
        $plane['data']= $arr;


        $prices = \Stripe\Price::all();

        //dd($plane);

        $currentSub = [];
        if (auth()->user()->stripe_id != '') {
            $sub = \Stripe\Customer::retrieve([
                'id' => auth()->user()->stripe_id,
                'expand' => ['subscriptions']
            ]);

            foreach ($sub->subscriptions->data as $data) {
                $currentSub[] = array('plan' => $data->plan->id, 'sub' => $data->id, 'next' => date('d/m/Y', $sub->subscriptions->data[0]['current_period_end']));
            }
        }

        return view('subscription.select',
            compact('plane', 'currentSub', 'prods', 'prices')
        );
    }

    public function orderPost(Request $request)
    {

        $user = auth()->user();
        $input = $request->all();
        $token =  $request->stripeToken;
        $paymentMethod = $request->paymentMethod;

        try {

            Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

            if (is_null($user->stripe_id)) {
                $stripeCustomer = $user->createAsStripeCustomer();

                $customer = \Stripe\Customer::create(['email' => $user->email]);

                $user->update(['stripe_customer_id' => $customer->id]);
            }

            \Stripe\Customer::createSource(
                $user->stripe_id,
                ['source' => $token]
            );

            //dd($input['plane']);

            $user->newSubscription('test',$input['plane'])
                ->create($paymentMethod, [
                    'email' => $user->email,
                ]);


            auth()->user()->subscription = $input['plane'];
            auth()->user()->save();

            return back()->with('success','Subscription is completed.');
        } catch (Exception $e) {
            return back()->with('success',$e->getMessage());
        }

    }

    public function cancel(Request $request)
    {
        $user = auth()->user();
        $input = $request->all();
        $token =  $request->stripeToken;

        try {

            Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

            $subscription = \Stripe\Subscription::retrieve($input['sub-id']);
            $subscription->cancel();

            auth()->user()->subscription = null;
            auth()->user()->save();



            return back()->with('success','Subscription is canceled.');
        } catch (Exception $e) {
            return back()->with('success',$e->getMessage());
        }


    }



}
