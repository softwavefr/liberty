<?php

namespace App\Http\Controllers;

use App\Models\Chat;
use App\Repositories\ChatRepository;
use App\Repositories\NewsRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Events\MessageSent;

class ChatsController extends Controller
{

    /**
     * Ad repository.
     *
     * @var App\Repositories\AdRepository
     */
    protected $chatRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ChatRepository $chatRepository)
    {
        $this->chatRepository = $chatRepository;
        $this->middleware('auth');
    }


    /**
     * Show chats
     *
     * @return \Illuminate\Http\Response
     */
    public function index($user = 0)
    {

        $writer = $this->chatRepository->listWriter(auth()->user()->id, $limit=10);

        return view('chat.chat', compact('user', 'writer'));
    }

    /**
     * Fetch all messages
     *
     * @return Chat
     */
    public function fetchMessages(Request $request)
    {
        if(!$request->to_user ) {
            debug("no msg");
            return;
        }

        return $this->chatRepository->search(auth()->user()->id, $request->to_user);
    }

    /**
     * Persist message to database
     *
     * @param  Request $request
     * @return Response
     */
    public function sendMessage(Request $request)
    {

        if(!$request->input('to_user') || !$request->input('message') ) {
            debug("no msg");
            return;
        }

        $message = new Chat();
        $message->from_user = Auth::user()->id;
        $message->to_user = $request->input('to_user');
        $message->message = $request->input('message');
        $message->save();

        return ['status' => 'Message Sent!'];

    }
}
