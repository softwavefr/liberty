<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Srmklive\PayPal\Services\ExpressCheckout;
use App\Models\Invoice;
use DateTime;

class PaypalController extends Controller
{
    protected $provider;
    public function __construct() {
        $this->provider = new ExpressCheckout();
    }

    public function index()
    {

        $mode_children = false;
        if (auth()->check() == true){
            $mode_children = auth()->user()->mode_children;
        }

        $invoice = Invoice::select('id', 'title','created_at', 'recurring_id', 'subscription')
            ->where('user_id', '=', auth()->user()->id)
            ->where('payment_status', '<>', 'Canceled')
            ->where('payment_status', '<>', 'NoActived')
            ->orderBy('created_at', 'desc')->limit(1)
            ->get()->toArray();

        $abo = [];
        if ($invoice && $invoice[0]['created_at'] != '') {

            $end_at = new DateTime($invoice[0]['created_at']);
            $end_at->modify('1 month');

            $abo = [
                'id' =>  $invoice[0]['id'],
                "title" => $invoice[0]['title'],
                "start" => $invoice[0]['created_at'],
                "reccuring" => $invoice[0]['recurring_id'],
                "end" => $end_at->format('Y-m-d'),
                "subscription" => $invoice[0]['subscription'],

            ];
        }

        return view('admin.payment', compact('abo', 'mode_children'));
    }

    public function expressCheckout(Request $request) {

        /*
        N9TT-9G0A-B7FQ-RANC -> visiteur -> 1€
        QK6A-JI6S-7ETR-0A6C -> bronze -> 2€
        SXFP-CHYK-ONI6-S89U -> argent -> 4€
        XNSS-HSJW-3NGU-8XTJ -> or -> 5€
        NHLE-L6MI-4GE4-ETEV
        6ETI-UIL2-9WAX-XHYO
        2E62-E3SR-33FI-XHV3
        7EIQ-72IU-2YNV-3L4Y
        */

        $option = $request->input('option');

        // Get the cart data
        $cart = $this->getCart($option);


        // get new invoice id
        $invoice_id = Invoice::count() + 1;

        // create new invoice
        $invoice = new Invoice();
        $invoice->id = $invoice_id;
        $invoice->user_id = auth()->user()->id;
        $invoice->title = $cart['items'][0]['desc'];
        $invoice->price = $cart['items'][0]['price'];
        $invoice->subscription = $option;
        $invoice->payment_status = 'NoActived';
        $invoice->save();


        $data = [];
        $data['items'] = $cart['items'];
        $data['invoice_id'] = $invoice_id;
        $data['invoice_description'] = $cart['items'][0]['desc'];
        $data['return_url'] = url('/paypal/express-checkout-success');
        $data['cancel_url'] = url('/abonnement');
        $data['total'] = $cart['items'][0]['price'];




        // send a request to paypal
        // paypal should respond with an array of data
        // the array should contain a link to paypal's payment system
        $response = $this->provider->setExpressCheckout($data, $cart['items'][0]['recurring']);

        // if there is no link redirect back with error message
        if (!$response['paypal_link']) {
            return redirect('/paypal')->with(['code' => 'danger', 'message' => 'Something went wrong with PayPal !']);
            // For the actual error message dump out $response and see what's in there
        }

        // redirect to paypal
        // after payment is done paypal
        // will redirect us back to $this->expressCheckoutSuccess
        return redirect($response['paypal_link']);
    }

    // private function getCart($price, $recurring, $invoice_id)
    private function getCart($code)
    {

        $data = [];
        if ($code == 'N9TT-9G0A-B7FQ-RANC')
        {
            return [
                'items' => [
                    [
                        'name' => 'N9TT-9G0A-B7FQ-RANC',
                        'price' => 1,
                        'desc' => 'inscription pour 30 jours SIMPLE',
                        'qty' => 1,
                        'recurring' => false,
                    ]
                ],
                'total' => 1,
                'invoice_description' => 'inscription SIMPLE',
                'subscription_desc' => 'inscription SIMPLE',
            ];
        }
        elseif ($code == 'QK6A-JI6S-7ETR-0A6C')
        {
            return [
                'items' => [
                    [
                        'name' => 'QK6A-JI6S-7ETR-0A6C',
                        'price' => 2,
                        'desc' => 'abonnement mensuel BRONZE',
                        'qty' => 1,
                        'recurring' => true,
                    ]
                ],
                'total' => 2,
                'invoice_description' => 'abonnement mensuel BRONZE',
                'subscription_desc' => 'abonnement mensuel BRONZE',

            ];
        }
        elseif ($code == 'SXFP-CHYK-ONI6-S89U')
        {
            return [
                'items' => [
                    [
                        'name' => 'SXFP-CHYK-ONI6-S89U',
                        'price' => 4,
                        'desc' => 'abonnement mensuel ARGENT',
                        'qty' => 1,
                        'recurring' => true,
                    ]
                ],
                'total' => 4,
                'invoice_description' => 'abonnement mensuel ARGENT',
                'subscription_desc' => 'abonnement mensuel ARGENT',
            ];
        }
        elseif ($code == 'XNSS-HSJW-3NGU-8XTJ')
        {
            return [
                'items' => [
                    [
                        'name' => 'XNSS-HSJW-3NGU-8XTJ',
                        'price' => 5,
                        'desc' => 'abonnement mensuel OR',
                        'qty' => 1,
                        'recurring' => true,
                    ]
                ],
                // total is calculated by multiplying price with quantity of all cart items and then adding them up
                // in this case total is 20 because price is 20 and quantity is 1
                'total' => 5,
                'invoice_description' => 'abonnement mensuel OR',
                'subscription_desc' => 'abonnement mensuel OR',
            ];
        }

/*
        if ($recurring) {
            return [
                // if payment is recurring cart needs only one item
                // with name, price and quantity
                'items' => [
                    [
                        'name' => 'Abonnement mensuel de Liberty ' . config('paypal.invoice_prefix') . ' #' . $invoice_id,
                        'price' => $price,
                        'qty' => 1,
                    ],
                ],


                'subscription_desc' => 'Monthly Subscription ' . config('paypal.invoice_prefix') . ' #' . $invoice_id,
                // every invoice id must be unique, else you'll get an error from paypal
                'invoice_id' => config('paypal.invoice_prefix') . '_' . $invoice_id,
                'invoice_description' => "Order #". $invoice_id ." Invoice",



            ];
        }

        return [
            // if payment is not recurring cart can have many items
            // with name, price and quantity
            'items' => [
                [
                    'name' => 'bronze',
                    'price' => 2,
                    'qty' => 1,
                ],
                [
                    'name' => 'argent',
                    'price' => 4,
                    'qty' => 1,
                ],
                [
                    'name' => 'or',
                    'price' => 5,
                    'qty' => 1,
                ],


            ],


            // every invoice id must be unique, else you'll get an error from paypal
            'invoice_id' => config('paypal.invoice_prefix') . '_' . $invoice_id,
            'invoice_description' => "Order #" . $invoice_id . " Invoice",
            'cancel_url' => url('/paypal'),
            // total is calculated by multiplying price with quantity of all cart items and then adding them up
            // in this case total is 20 because Product 1 costs 10 (price 10 * quantity 1) and Product 2 costs 10 (price 5 * quantity 2)
            'total' => $price,
        ];
            */
    }

    public function expressCheckoutSuccess(Request $request) {




        // check if payment is recurring
        $recurring = $request->input('recurring', false) ? true : false;
        $price = $request->input('price');

        $token = $request->get('token');
        $PayerID = $request->get('PayerID');

        // initaly we paypal redirects us back with a token
        // but doesn't provice us any additional data
        // so we use getExpressCheckoutDetails($token)
        // to get the payment details
        $response = $this->provider->getExpressCheckoutDetails($token);
        $code = $response['L_NAME0'];

        // if response ACK value is not SUCCESS or SUCCESSWITHWARNING
        // we return back with error
        if (!in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {
            return redirect('/')->with(['code' => 'danger', 'message' => 'Error processing PayPal payment']);
        }

        // invoice id is stored in INVNUM
        // because we set our invoice to be xxxx_id
        // we need to explode the string and get the second element of array
        // witch will be the id of the invoice
        $invoice_id = explode('_', $response['INVNUM']);

        // get cart data
        $cart = $this->getCart($code);

        // check if our payment is recurring
        $status = 'Invalid';
        $transaction_id = 0;
        $profil_id = 0;

        $cart['invoice_id'] = $invoice_id;

        if ($cart['items'][0]['recurring'] === true) {

            // if recurring then we need to create the subscription
            // you can create monthly or yearly subscriptions
            $response = $this->provider->createMonthlySubscription($response['TOKEN'], $response['AMT'], $cart['subscription_desc']);
            $profil_id = $response['PROFILEID'];

            // if after creating the subscription paypal responds with activeprofile or pendingprofile
            // we are good to go and we can set the status to Processed, else status stays Invalid
            if (!empty($response['PROFILESTATUS']) && in_array($response['PROFILESTATUS'], ['ActiveProfile', 'PendingProfile'])) {
                $status = 'Processed';
            }

        } else {

            // if payment is not recurring just perform transaction on PayPal
            // and get the payment status
            $payment_status = $this->provider->doExpressCheckoutPayment($cart, $token, $PayerID);

            $status = $payment_status['PAYMENTINFO_0_PAYMENTSTATUS'];
            $transaction_id = $payment_status['PAYMENTINFO_0_TRANSACTIONID'];

        }


        // find invoice by id
        $invoice = Invoice::find($invoice_id)->first();

        // set invoice status
        $invoice->payment_status = $status;

        // if payment is recurring lets set a recurring id for latter use
        if ($cart['items'][0]['recurring'] === true) {
            $invoice->recurring_id = $profil_id;
        }
        else
            {

                $invoice->transaction_id = $transaction_id;
        }


        // save the invoice
        $invoice->save();

        auth()->user()->subscription = $code;
        auth()->user()->save();

        // App\Invoice has a paid attribute that returns true or false based on payment status
        // so if paid is false return with error, else return with success message




        if ($invoice->paid) {
            return redirect('/abonnement')->with(['code' => 'success', 'message' => 'Order ' . $invoice->id . ' has been paid successfully!']);
        }

        return redirect('/abonnement')->with(['code' => 'danger', 'message' => 'Error processing PayPal payment for Order ' . $invoice->id . '!']);
    }


    public function expressCancel($id=0)
    {

        $invoice = Invoice::select()
            ->where('id', $id)
            ->get()->toArray();


        if ($invoice[0]['recurring_id'] != '')
        {
                //$data = $this->provider->getRecurringPaymentsProfileDetails($invoice[0]['recurring_id']);
                $ret = $this->provider->cancelRecurringPaymentsProfile($invoice[0]['recurring_id']);

                if ($ret['ACK'] == 'Success') {
                    $invoice = Invoice::where('recurring_id', '=', $invoice[0]['recurring_id'])
                        ->where('user_id', '=', auth()->user()->id)
                        ->update(['payment_status' => 'Canceled']);

                    auth()->user()->subscription = null;
                    auth()->user()->save();
                }
                else
                {
                    return redirect('/abonnement')->with(['code' => 'danger', 'message' => $ret['L_SHORTMESSAGE0']]);
                }
        }
        else
        {
            $invoice = Invoice::where('user_id', '=', auth()->user()->id)
                ->where('id', '=', $id)
                ->update(['payment_status' => 'Canceled']);

            auth()->user()->subscription = null;
            auth()->user()->save();

        }

        return redirect('/abonnement')->with(['code' => 'success', 'message' => 'Order ' . $invoice[0]['title'] . ' has been paid canceled!']);


    }
}
