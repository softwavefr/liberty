<?php

namespace App\Http\Controllers;


use App\Repositories\ParameterRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Category;
use App\Models\Region;
use App\Models\Ad;
use App\Models\User;
use App\Model\Parameter;

use App\Repositories\AdRepository;
use App\Repositories\NewsRepository;

use App\Http\Requests\AdStore;
use Carbon\Carbon;
use App\Models\Upload;

use App\Http\Requests\AdUpdate;

class AdController extends Controller
{

    /**
     * Ad repository.
     *
     * @var App\Repositories\AdRepository
     */
    protected $adRepository;
    protected $newsRepository;
    protected $parameterRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(AdRepository $adRepository, NewsRepository $newsRepository, ParameterRepository $parameterRepository)
    {
        $this->adRepository = $adRepository;
        $this->newsRepository = $newsRepository;
        $this->parameterRepository = $parameterRepository;
    }



    /**
     * affiche la liste de 8 annonces sur la page d'accueil
     * affiche les articles
     */
    public function home()
    {
        $mode_children = false;
        if (auth()->check() == true){
            $mode_children = auth()->user()->mode_children;
        }

        $ads = $this->adRepository->last(8);
        $adsClub = $this->adRepository->club(10);
        $news = $this->newsRepository->last(10, 'news');
        $coverage = $this->newsRepository->last(1, 'coverage');
        $video_param = $this->parameterRepository->getParam('', 'VIDEO')->get()->toArray();
        $video = $video_param[0]['value'];

        $data = Category::select('code', 'id')->oldest('name')->get()->toArray();
        $categories = array();
        foreach ($data as $item) {
            $categories[$item['id']] = $item['code'];
        }

        $data = Region::select('id','code','name','slug')->oldest('name')->get()->toArray();
        $regions = array();

        foreach ($data as $item) {
            $regions[$item['slug']] = $item['name'];
        }

        return view('home', compact('ads', 'adsClub', 'categories', 'regions', 'news', 'coverage', 'mode_children', 'video'));
    }


    /**
     * Search ads.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  String  $slug
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
       setlocale (LC_TIME, 'fr_FR');
       $ads = $this->adRepository->search($request);

       return view('partials.ads', compact('ads'));
    }

    /**
     * Affiche la liste des annonces sur la page annonces
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  String  $regionSlug
     * @param  Integer  $departementCode
     * @param  Integer  $communeCode
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $regionSlug = null, $departement = null, $commune = null)
    {

        $category = 0;
        $categories = Category::select('name', 'id')->oldest('name')->get();
        $regions = Region::select('id','code','name','slug')->oldest('name')->get();

        if ($request->input('region') !== '')
            $regionSlug = $request->input('region');

        if ($request->input('category') !== null)
            $category = $request->input('category');

        if ($request->input('departement') !== null)
            $departement = $request->input('departement');

        if ($request->input('commune') !== null)
            $commune = $request->input('commune');

        $region = $regionSlug ? Region::whereSlug($regionSlug)->firstOrFail() : 0;

        $page = $request->query('page', 0);

        return view('ads.list', compact('categories', 'regions', 'region', 'departement', 'commune', 'page', 'category'));
    }

    /**
     * affiche la page pour creer une annonce
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(!$request->session()->has('index')) {
            $request->session()->put('index', Str::random(30));
        }
        $categories = Category::select('name', 'id')->oldest('name')->get();

        $regions = Region::oldest('name')->get();
        return view('ads.create', compact('categories', 'regions'));
    }

    /**
     * enregistre la nouvelle annonce
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdStore $request)
    {
        $commune = json_decode(file_get_contents('https://geo.api.gouv.fr/communes/' . $request->commune), true);
        $ad = $this->adRepository->create([
            'title' => $request->title,
            'texte' => $request->texte,
            'telephone' => $request->phone,
            'category_id' => $request->category,
            'region_id' => $request->region,
            'departement' => $request->departement,
            'commune' => $request->commune,
            'commune_name' => $commune['nom'],
            'commune_postal' => $commune['codesPostaux'][0],
            'prix' => $request->prix,
            'adresse' => $request->adresse,
            'user_id' => auth()->check() ? auth()->id() : 0,
            'pseudo' => auth()->check() ? auth()->user()->name :$request->pseudo,
            'email' => auth()->check() ? auth()->user()->email : $request->email,
            'limit' => Carbon::now()->addWeeks($request->limit),

        ]);

        if($request->session()->has('index')) {
            $index = $request->session()->get('index');

            Upload::whereIndex($index)->update(['ad_id' => $ad->id, 'index' => 0]);
        }
        return view('adconfirm');
    }

    /**
     * affiche une annonce
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Ad $ad)
    {
        $this->authorize('show', $ad);
        $photos = $this->adRepository->getPhotos($ad);
        return view('ads.show', compact('ad', 'photos'));
    }


    /**
     * affiche la page pour modifier une annonce
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Ad $ad)
    {
        //$this->authorize('show', $ad);
        return view('ads.edit', compact('ad'));
    }

    /**
     * modifie une annonce
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdUpdate $request, Ad $ad)
    {
        $this->authorize('manage', $ad);
        $this->adRepository->update($ad, $request->all());
        $request->session()->flash('status', "L'annonce a bien été modifiée.");
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * Display a listing actualité.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  String  $regionSlug
     * @param  Integer  $departementCode
     * @param  Integer  $communeCode
     * @return \Illuminate\Http\Response
     */
    public function doPage()
    {

       return view('doPage');
    }



}
