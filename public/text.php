<!DOCTYPE html>
<html>
<head>

    <link rel="stylesheet" href="trumbowyg/dist/ui/trumbowyg.min.css">
    <!-- Import jQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.3.1.min.js"><\/script>')</script>

    <!-- Import Trumbowyg -->
    <script src="trumbowyg/dist/trumbowyg.min.js"></script>



</head>

<body>

<!-- Wrap the editor with an element with the class trumbowyg-dark -->
<div class="trumbowyg-dark">
    <textarea id="editor"></textarea>
</div>



<script>

    $('#editor').trumbowyg();
</script>



</body>
</html>
