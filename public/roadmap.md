
# Roadmap du projet Liberty


## environnement
* debian 10
* apache 2.4.38
* codage en php 7.3.11
* base de données MariaDb 10.4.8
* Laravel 5.8

##### hébergement
* ~~les sources sous sous gitLab~~
* prendre un hebergement
* compte strike


## les fonctionnalités 

##### lot1 
* ~~crud classique de l'annonce~~
* créer annonces / catégorie
* ~~gestion utilisateur~~
* ~~répondre a l'annonce par email~~
* ~~modération par un admin~~

* paiement compte mensuel par strike 19 € / mois

##### lot2
* chat en direct entre les abonnés
* laisser un commentaire sur l'annonce



## page d'accueil
* menu haut
    * logo à faire
    * ~~bug fermeture menu sous smartphone~~
    * ~~lien vers les annonces / régions~~ 
    * lien vers la page actu 
    * ~~lien vers l'inscription~~
    * ~~lien vers la page de connexion~~

* affichage d'un carousel 
    * brancher le moteur de recherche

* ~~choix des annonces par lieux~~
    * ~~lien guadeloupe~~
    * ~~lien martinique~~
    * ~~lien guyane~~
    
* ~~affichage des dernières annonces~~
    * ~~affichage des 10 dernières annonces saisies~~
    * ~~lien sur l'affichage de l'annonce~~

* inscription evènement
    * faire la page hot party
    * brancher le lien

* ~~des annonces club~~ 
    * ~~afficher les annonces club~~
    * ~~faire un lien vers les annonces~~

* footer de page
    * ajouter les infos sociétés
    
## inscription / connexion

* ~~affichage d'un formulaire~~
* ~~desing de la maquette~~
* ~~authentification valide~~
* ~~autorisation fonctionnalité~~

## affichage des annonces par lieux
* ~~desing de la maquette~~
* remplacer les listes les listes par des buttons
* ne plus gérer les villes par ajax / webservices

## affichage d'une annonce 
* ~~desing de la maquette
* ~~afficher les données de l'annonce~~
* ~~afficher un tarif horaire / entrée~~
* ~~ajouter un numéro de téléphone~~
* discuter avec la personne
* ~~zoom des photos~~
* permettre de laisser un commentaire sur l'annonce


## création de l'annonce
* ~~desing de la maquette~~
* ~~ajouter des photos~~
* ~~saisie catégorie / titre / texte / semaine / lieu~~
* ~~créer le desing de la confirmation de l'annonce~~

## mon compte 
* ~~desing back office~~
* ~~on peut voir les annonces actives~~ 
* ~~en attente de validation~~
* ~~les anciennes annonces~~
* formulaire changement données personnels
    * ajouter plus de données
* ~~affichage des données pour le RGDP~~
* créer une page pour le paiement de son abonnement

## compte admin
* ~~l'admin doit modérer les annonces pour l'afficher sur le site~~
* ~~modérer les messages~~
* ~~un email est envoyé à l'utlisateur après modération~~

## paiement stripe
* ~~mettre en place le formulaire~~
* ~~créer le compte et les abonnements sur stripe~~ (note il faut le compte du client)
* on peut prendre 3 abonnements (1€ / jour, 19€ / mois et 10 € / mois) (à changer)
* controle le bon fonctionnenemnt avant la mise en prod
* ne pouvoir s'abonner qu'a un seul abonnement à la fois
* pouvoir annuler son abonnement
* créer la politique de fonctionnenennt 
* et explication sur les services 

 

 
     
