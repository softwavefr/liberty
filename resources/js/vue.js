window.Vue = require('vue');

/*
const app = new Vue({
    el: '#app'
});
*/

require('./bootstrap');

Vue.component('ad', require('./components/AdComponent.vue').default);
Vue.component('chat-messages', require('./components/ChatMessages.vue').default);
Vue.component('chat-form', require('./components/ChatForm.vue').default);
Vue.component('mon-composant', {
    template: '<p> composant de test</p>'
});

var ufrom = $("#userfrom").val();
var uto = $("#userto").val();

const app = new Vue({
    el: '#app',
    data: {
        messages: [],
        userfrom: ufrom,
        userto: uto
    },


    created() {

        this.fetchMessages();
        setInterval(this.fetchMessages, 1000);
    },

    methods: {
        fetchMessages() {

            axios.get('/chats', { params: { to_user: this.userto } }).then(response => {
                this.messages = response.data;
            });
        },

        addMessage(message) {
            this.messages.push(message);
            // this.userto.push(message);

            // axios.post('/chats', { params: { message: message.message, to_user: this.userto } }).then(response => {
            axios.post('/chats', { 'message': message.message, 'to_user': this.userto }).then(response => {
                console.log(response.data);
            });

        }
    }
});

