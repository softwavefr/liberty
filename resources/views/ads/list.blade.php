@extends('layouts.app')
@section('content')

    <div id="app">
        @if($region)
            <div id="start"
                 data-category="{{ $category }}"
                 data-id={{ $region->id }}
                 data-code="{{ $region->code }}"
                 data-slug="{{ $region->slug }}"
                 @if($departement)
                   data-departement="{{ $departement }}"
                     @if($commune)
                       data-commune="{{ $commune }}"
                     @endif
                 @endif
        @else
                <div id="start"
                     data-category="{{ $category }}"
                     data-id=0


        @endif

        @if($page != 0)
          data-page="{{ $page }}"
        @endif>

    </div>
        <ad url="{{ route('annonces.search') }}" :categories="{{ $categories }}" :regions="{{ $regions }}" :category="{{ $category }}" ref="adComp"></ad>

@endsection
@section('script')
<script src="{{ asset('js/vue.js') }}"></script>
@endsection
