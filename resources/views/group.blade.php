@extends('layouts.app')
@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-7 col-xl-5 px-4 pb-4 pl-xl-5 pr-xl-5">
                <section class="mx-n4 mx-xl-n5 mb-4 mb-xl-5">
                    <!-- Slider main container-->
                    <div class="swiper-container booking-detail-slider swiper-container-horizontal" style="width: 94%">
                        <!-- Additional required wrapper-->
                        <div class="swiper-wrapper" style="transition-duration: 0ms; transform: translate3d(-162.75px, 0px, 0px);">
                            <div class="swiper-slide swiper-slide-duplicate swiper-slide-prev" data-swiper-slide-index="4" >
                                <img src="/img/instagram/1.jpg" alt="Bedroom" class="img-fluid">
                            </div>
                            <div class="swiper-slide swiper-slide-duplicate swiper-slide-active" data-swiper-slide-index="5" >
                                <img src="/img/instagram/1.jpg" alt="Bedroom" class="img-fluid">
                            </div>
                            <!-- Slides-->
                            <div class="swiper-slide swiper-slide-next" data-swiper-slide-index="0" >
                                <img src="/img/instagram/3.jpg" alt="Our street" class="img-fluid">
                            </div>
                            <div class="swiper-slide" data-swiper-slide-index="1" >
                                <img src="/img/instagram/4.jpg" alt="Outside" class="img-fluid">
                            </div>
                            <div class="swiper-slide" data-swiper-slide-index="2" >
                                <img src="/img/instagram/5.jpg" alt="Rear entrance" class="img-fluid">
                            </div>
                        </div>
                        <div class="swiper-pagination swiper-pagination-white swiper-pagination-clickable swiper-pagination-bullets swiper-pagination-bullets-dynamic" style="width: 80px;"><span
                                class="swiper-pagination-bullet" style="left: -48px;" tabindex="0" role="button" aria-label="Go to slide 1"></span><span class="swiper-pagination-bullet"
                                                                                                                                                         style="left: -48px;" tabindex="0" role="button"
                                                                                                                                                         aria-label="Go to slide 2"></span><span
                                class="swiper-pagination-bullet" style="left: -48px;" tabindex="0" role="button" aria-label="Go to slide 3"></span><span
                                class="swiper-pagination-bullet swiper-pagination-bullet-active-prev-prev" style="left: -48px;" tabindex="0" role="button" aria-label="Go to slide 4"></span><span
                                class="swiper-pagination-bullet swiper-pagination-bullet-active-prev" style="left: -48px;" tabindex="0" role="button" aria-label="Go to slide 5"></span><span
                                class="swiper-pagination-bullet swiper-pagination-bullet-active swiper-pagination-bullet-active-main" style="left: -48px;" tabindex="0" role="button"
                                aria-label="Go to slide 6"></span></div>
                        <div class="swiper-button-prev swiper-button-white" tabindex="0" role="button" aria-label="Previous slide"></div>
                        <div class="swiper-button-next swiper-button-white" tabindex="0" role="button" aria-label="Next slide"></div>
                        <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
                </section>
                <!-- Breadcrumbs -->

                <div class="text-block">
                    <p class="subtitle">WORLD'CONNEXION</p>
                    <h1 class="hero-heading mb-3">NOTRE COMMUNAUTÉ </h1>
                </div>
                <div class="text-block">
                    <h6 class="mb-4">Liste des evènnements WORLD'CONNEXION </h6>
                    <div class="row mb-3">
                        <div class="col-md-6 d-flex align-items-center mb-3 mb-md-0">
                            <div class="date-tile mr-3">
                                <div class="text-uppercase"><span class="text-sm">PAS D'EVENT</span><br></div>
                            </div>

                        </div>


                    </div>
                </div>


                <div class="text-block">
                    <h6 class="mb-3">TOUTE L'EQUIPE WORLD'CONNEXION VOUS REMERCIE </h6>
                    <div class="row mb-3">
                        <div class="col-auto text-center text-sm"></div>
                        <div class="col-auto text-center text-sm"></div>
                        <div class="col-auto text-center text-sm"></div>
                    </div>
                </div>


                <div class="text-block d-print-none">
                    <button onclick="window.print()" class="btn btn-link pl-0"><i class="fa fa-print mr-2"></i>Imprimer en cas d'évent</button>
                </div>
            </div>
            <div class="col-lg-5 col-xl-7 map-side-lg px-lg-0">
                <div id="detailSideMap" class="map-full shadow-left leaflet-container leaflet-touch leaflet-fade-anim leaflet-grab leaflet-touch-drag leaflet-touch-zoom" style="position: relative;"
                     tabindex="0">

                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')



    <!-- Map-->
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ==" crossorigin="" />
    <style type="text/css">
        #map{ /* la carte DOIT avoir une hauteur sinon elle n'apparaît pas */
            height:400px;
        }
    </style>
    <script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js" integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw==" crossorigin=""></script>
    <script>


        var lat = 48.852969;
        var lon = 2.349903;
        var macarte = null;
        // Nous initialisons une liste de marqueurs
        var villes = {
            "Paris": { "lat": 48.852969, "lon": 2.349903 },
            "Brest": { "lat": 48.383, "lon": -4.500 },
            "Quimper": { "lat": 48.000, "lon": -4.100 },
            "Pointe-à-Pitre": { "lat": 16.2333 , "lon": 61.5167 },
            "Marseille": { "lat": 43.3, "lon": 5.4 },
            "Sablons": { "lat": 45.0333, "lon":  0.1833},
            "Le diamant": { "lat": 14.4667, "lon": 61.0333 },
            "Ducos": { "lat": 14.5667 , "lon": 60.9833 },
            "Lyon": { "lat": 45.750000, "lon": 4.850000 },
            "Les Abymes": { "lat": 16.27 , "lon": -61.52 },
            "Nantes": { "lat": 47.2173, "lon": 1.5534 }
        };
        // Fonction d'initialisation de la carte
        function initMap() {
            // Créer l'objet "macarte" et l'insèrer dans l'élément HTML qui a l'ID "map"
            macarte = L.map('detailSideMap').setView([lat, lon], 5);
            // Leaflet ne récupère pas les cartes (tiles) sur un serveur par défaut. Nous devons lui préciser où nous souhaitons les récupérer. Ici, openstreetmap.fr
            L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
                // Il est toujours bien de laisser le lien vers la source des données
                attribution: 'données © <a href="//osm.org/copyright">OpenStreetMap</a>/ODbL - rendu <a href="//openstreetmap.fr">OSM France</a>',
                minZoom: 1,
                maxZoom: 20
            }).addTo(macarte);
            // Nous parcourons la liste des villes
            for (ville in villes) {
                var marker = L.marker([villes[ville].lat, villes[ville].lon]).addTo(macarte);
            }
        }
        window.onload = function(){
            // Fonction d'initialisation qui s'exécute lorsque le DOM est chargé
            initMap();
        };


    </script>

@endsection
