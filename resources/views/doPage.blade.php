@extends('layouts.app')

@section('content')

    <div class="row">

        <div style="background-image: url('img/photo/photo-couple.jpg'); width: 1668px;"
             class="swiper-slide bg-cover dark-overlay swiper-slide-active swiper-slide-duplicate-next swiper-slide-duplicate-prev" data-swiper-slide-index="0">
            <br/><br/>
            <div class="container h-100">
                <div data-swiper-parallax="-500" class="d-flex h-100 text-white overlay-content align-items-center" style="transition-duration: 0ms; transform: translate3d(0px, 0px, 0px);">
                    <div class="w-100">
                        <div class="row">
                            <div class="col-lg-6">
                                <p class="subtitle text-white letter-spacing-3 mb-3 font-weight-light">
                                    <marquee><strong>DECOUVREZ BIENTÔT DES ARTICLES INÉDIT</strong></marquee>
                                </p>
                                <h2 style="line-height: 1" class="display-3 font-weight-bold mb-3">Le libertinage, est-ce fait pour moi ?</h2>
                                <p class="mb-5">Que ce soit pour tenter de nouvelles expériences, pour se sentir désiré par quelqu’un d’autre, pour pimenter sa vie sexuelle ou simplement assouvir sa
                                    curiosité, les raisons sont multiples chez ceux qui s'adonnent au libertinage. Mais les manières de le pratiquer diffèrent tout autant. Le couple peut en convier un
                                    autre chez lui en toute intimité, participer à des ébats avec un groupe de personnes, faire l'amour à côté d’un autre couple (le côte-à-côtisme), le faire chacun de
                                    son côté ou l'un des partenaire peut simplement regarder sa moitié avoir des relations sexuelles avec quelqu'un d'autre (le caudalisme). Le couple peut simplement
                                    regarder, participer, se montrer ou partager.</p><a href="#" class="btn btn-outline-light d-none d-sm-inline-block"><i class="fa fa-angle-right ml-2"></i></a>
                            </div>
                            <div class="col-lg-6 pl-lg-5 my-3 my-md-5 my-lg-0"><a href="#" class="media text-reset text-decoration-none hover-animate mb-2 mb-md-5">
                                    <div class="icon-rounded bg-white opacity-7 mr-4">
                                        <svg class="svg-icon text-dark w-2rem h-2rem">
                                            <use xlink:href="#love-pin-1"></use>
                                        </svg>
                                    </div>
                                    <div class="media-body">
                                        <h5></h5>
                                        <div class="badge badge-light">Ecouter la version audio de l'article</div>
                                    </div>
                                </a><a href="#" class="media text-reset text-decoration-none hover-animate mb-2 mb-md-5">
                                    <div class="icon-rounded bg-white opacity-7 mr-4">
                                        <svg class="svg-icon text-dark w-2rem h-2rem">
                                            <use xlink:href="#headphones-1"></use>
                                        </svg>
                                    </div>
                                    <div class="media-body">
                                        <h5>Audio bientôt disponible</h5>
                                        <div class="badge badge-light">Ecouter la version audio de l'article</div>
                                    </div>
                                </a><a href="#" class="media text-reset text-decoration-none hover-animate">
                                    <div class="icon-rounded bg-white opacity-7 mr-4">
                                        <svg class="svg-icon text-dark w-2rem h-2rem">
                                            <use xlink:href="#camera-shutter-1"></use>
                                        </svg>
                                    </div>
                                    <div class="media-body">
                                        <h5>Article vidéo</h5>
                                        <p>Cet article contient de la vidéo </p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br/><br/>
        </div>
    </div>

@endsection
