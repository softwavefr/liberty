@extends('layouts.app')
@section('content')

    <style>
        .alert.parsley {
            margin-top: 5px;
            margin-bottom: 0px;
            padding: 10px 15px 10px 15px;
        }

        .check .alert {
            margin-top: 20px;
        }

        .credit-card-box .panel-title {
            display: inline;
            font-weight: bold;
        }

        .credit-card-box .display-td {
            display: table-cell;
            vertical-align: middle;
            width: 100%;
            text-align: center;
        }

        .credit-card-box .display-tr {
            display: table-row;
        }
    </style>

    <section class="py-6">
        <div class="container">
            <div class="row">
                <div class="panel panel-default credit-card-box col-md-10">
                    <div class="panel-heading">
                        <div class="row">
                            <h3 class="panel-title">Confirmation d'abonnement</h3>
                        </div>
                    </div>
                    <div class="panel-bodyy">
                        <div class="col-md-12">
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success alert-block">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{ $message }}</strong>
                                </div>
                            @endif

                            @isset($_POST['plane-id'])
                                    {!! Form::open(['url' => route('order-post'), 'data-parsley-validate', 'id' => 'payment-form']) !!}
                            <div class="form-group" id="product-group">
                                <div class="col-md-4">
                                    <div class="subscription-option">
                                        <input type="hidden" id="plane" name="plane" value='{{$_POST['plane-id']}}'>
                                        <label for="plan-silver">
                                            <span class="plan-nam">Abonnement: {{$_POST['plane-name']}}</span>
                                            <span class="plan-price">à {{$_POST['plane-amount']}} €<small> /{{$_POST['plane-interval']}}</small></span>

                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div id="card-element"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button id="card-button" class="btn btn-lg btn-block btn-primary btn-order">Validation</button>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <span class="payment-errors" id="card-errors" style="color: red;margin-top:10px;"></span>
                                </div>
                            </div>
                                {!! Form::close() !!}
                            @else
                                <div class="form-group">
                                    <a class="nav-link" href=" {{ route('abonnement') }}">
                                        <button class="btn btn-lg btn-block btn-primary btn-order">Retour</button>
                                    </a>
                                </div>
                            @endif

                        </div>
                    </div>
                </div>

            </div>
        </div>

    </section>

        <!-- PARSLEY -->
        <script>
            window.ParsleyConfig = {
                errorsWrapper: '<div></div>',
                errorTemplate: '<div class="alert alert-danger parsley" role="alert"></div>',
                errorClass: 'has-error',
                successClass: 'has-success'
            };
        </script>

        <script src="http://parsleyjs.org/dist/parsley.js"></script>

        <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
        <script src="https://js.stripe.com/v3/"></script>
        <script>
            var style = {
                base: {
                    color: '#32325d',
                    lineHeight: '18px',
                    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                    fontSmoothing: 'antialiased',
                    fontSize: '16px',
                    '::placeholder': {
                        color: '#aab7c4'
                    }
                },
                invalid: {
                    color: '#fa755a',
                    iconColor: '#fa755a'
                }
            };

            const stripe = Stripe('{{ env("STRIPE_KEY") }}', {locale: 'fr'}); // Create a Stripe client.
            const elements = stripe.elements(); // Create an instance of Elements.
            const card = elements.create('card', {
                style: style,
                hidePostalCode: true,
            }); // Create an instance of the card Element.

            card.mount('#card-element'); // Add an instance of the card Element into the `card-element` <div>.

            card.on('change', function (event) {
                var displayError = document.getElementById('card-errors');
                if (event.error) {
                    displayError.textContent = event.error.message;
                } else {
                    displayError.textContent = '';
                }
            });

            // Handle form submission.
            var form = document.getElementById('payment-form');


            var form = document.getElementById('payment-form');
            form.addEventListener('submit', function (event) {
                event.preventDefault();

                stripe.createToken(card).then(function (result) {
                    if (result.error) {
                        // Inform the user if there was an error.
                        var errorElement = document.getElementById('card-errors');
                        errorElement.textContent = result.error.message;
                    } else {
                        // Send the token to your server.
                        stripeTokenHandler(result.token);
                    }
                });
            });

            // Submit the form with the token ID.
            function stripeTokenHandler(token) {
                // Insert the token ID into the form so it gets submitted to the server
                var form = document.getElementById('payment-form');
                var hiddenInput = document.createElement('input');
                hiddenInput.setAttribute('type', 'hidden');
                hiddenInput.setAttribute('name', 'stripeToken');
                hiddenInput.setAttribute('value', token.id);
                form.appendChild(hiddenInput);

                // Submit the form
                form.submit();
            }
        </script>

@endsection
