@extends('layouts.app')
@section('content')

    <video autoplay width="100%" height="100%">
        <source src="/video/libert.mp4" type="video/mp4">
        Your browser does not support the HTML5 video tag.
    </video>

    <?php



    ?>

    <section class="py-6">
        <div class="container">
            <div class="row">

                @if ($message = Session::get('success'))
                    <div class="col-md-12">
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                    </div>
                @endif

                @foreach($plane as $plan)
                    @foreach($prods as $prod)

                        @if ($prod->id == $plan->product)

                        <div class="col-lg-4">
                            <div class="card mb-5 mb-lg-0 border-0 shadow">
                                <div class="card-body">
                                    <h2 class="text-base subtitle text-center text-primary py-3">{{$plan->nickname}}</h2>
                                    <p class="text-muted text-center"><span class="h1 text-dark">{{$plan->amount/100}}€</span><span class="ml-2">/ {{$plan->interval}}</span></p>
                                    <hr/>
                                    <ul class="fa-ul my-4">
                                        <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>{{ __('liberty.abonnement_'.$prod->metadata->mode) }}</li>
                                        <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>{{ $prod->metadata->pic }} Photos</li>
                                        <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>{{ $prod->metadata->text }} caractères</li>
                                        <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>{{ __('liberty.abonnement_chat_'.$prod->metadata->chat) }}</li>
                                    </ul>
                                </div>
                            </div>

                            <?php

                            $route = 'user.subscription.create';
                            $subscription = '';
                            foreach ($currentSub as $sub) {
                                $nextPayment = '';

                                if ($plan->id == $sub['plan']) {
                                    $route = 'user.subscription.cancel';
                                    $subscription = $sub['sub'];
                                    $nextPayment = $sub['next'];
                                }
                            }
                            ?>

                            {!! Form::open(['url' => route($route), 'data-parsley-validate', 'id' => 'payment-form']) !!}
                            <input type="hidden" name="sub-id" value='{{$subscription}}'>
                            <input type="hidden" name="plane-id" value='{{$plan->id}}'>
                            <input type="hidden" name="plane-name" value='{{$plan->nickname}}'>
                            <input type="hidden" name="plane-amount" value='{{$plan->amount/100}}'>
                            <input type="hidden" name="plane-interval" value='{{$plan->interval}}'>

                            <div class="form-group">
                                @if(in_array($plan->id, array_column($currentSub, 'plan') ))
                                    <button id="card-button" class="btn btn-lg btn-block btn-outline-danger btn-order">Annuler</button>
                                    <span class="small" style="color: #9a9696;">prochain prévelement {{ $nextPayment }}</span>
                                @elseif (count($currentSub) > 0)
                                    <div id="card-button" class="btn btn-lg btn-block btn-outline-primary btn-order disabled">Choisir</div>
                                @else
                                    <button id="card-button" class="btn btn-lg btn-block btn-outline-primary btn-order">Choisir</button>

                                @endif
                            </div>
                            {!! Form::close() !!}
                        </div>
                            @endif
                    @endforeach
                @endforeach

            </div>
        </div>
    </section>

@endsection

