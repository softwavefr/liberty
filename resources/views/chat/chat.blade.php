@extends('layouts.app')

@section('content')


    <div id="app" class="container py-5 px-4">
        <div class="row rounded-lg overflow-hidden shadow">
            <!-- Users box-->
            <div class="col-4 px-0">
                <div class="bg-white">

                    <div class="bg-gray px-4 py-2 bg-light">
                        <p class="h5 mb-0 py-1">Vos messages</p>
                    </div>

                    <input type="hidden" id="userfrom" value="{{ auth()->user()->id }}">
                    <input type="hidden" id="userto" value="{{ $user }}">

                    <div class="messages-box">
                        <div class="list-group rounded-0">
                            <?php foreach ($writer as $person) { ?>
                                <a href="/chat/{{ $person['from_user'] }}" class="list-group-item list-group-item-action rounded-0 <?php echo ($user == $person['from_user'] ? ' active text-white ' : '') ?>">
                                    <div class="media"><img src="https://res.cloudinary.com/mhmd/image/upload/v1564960395/avatar_usae7z.svg" alt="user" width="50" class="rounded-circle">
                                        <div class="media-body ml-4">
                                            <div class="d-flex align-items-center justify-content-between mb-1">
                                                <h6 class="mb-0"><?php echo $person['name'] ?></h6>
                                            </div>
                                        </div>
                                    </div>
                                </a>

                            <?php } ?>

                        </div>
                    </div>
                </div>
            </div>
            <!-- Chat Box -->

            <div class="col-8 px-0">

                <div class="panel-body">
                    <chat-messages
                        :messages="messages"
                        v-bind:userfrom="{{ auth()->user()->id }}"
                        v-bind:userto="{{ $user }}"
                    ></chat-messages>
                </div>

                <chat-form
                    v-on:messagesent="addMessage"
                    v-bind:userfrom="{{ auth()->user()->id }}"
                    v-bind:userto="{{ $user }}"
                ></chat-form>

            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('js/vue.js') }}"></script>
@endsection
