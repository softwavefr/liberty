@extends('layouts.app')
@section('content')

    <video autoplay  width="100%" height="100%">
        <source src="/video/libert.mp4" type="video/mp4">
        Your browser does not support the HTML5 video tag.
    </video>

    <section class="py-6">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <!-- pricing card-->
                    <div class="card mb-5 mb-lg-0 border-0 shadow">
                        <div class="card-body">
                            <h2 class="text-base subtitle text-center text-primary py-3">Visiteur</h2>
                            <p class="text-muted text-center"><span class="h1 text-dark">1€</span><span class="ml-2">/ mois</span></p>
                            <hr/>
                            <ul class="fa-ul my-4">
                                <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>voir les annonces et répondre</li>
                                <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>1 Photos</li>
                                <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>500 caractères</li>
                                <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>Messagerie</li>

                                <div class="text-center"><a href="#" class="btn btn-outline-primary">Choisir</a></div>

                            </ul>

                        </div>
                    </div>
                    <!-- /pricing card -->
                </div>


                <div class="col-lg-4">
                    <!-- pricing card-->
                    <div class="card mb-5 mb-lg-0 border-0 shadow">
                        <div class="card-body">
                            <h2 class="text-base subtitle text-center text-primary py-3">Bonze</h2>
                            <p class="text-muted text-center"><span class="h1 text-dark">2€</span><span class="ml-2">/ mois</span></p>
                            <hr/>
                            <ul class="fa-ul my-4">
                                <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>voir les annonces et répondre</li>
                                <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>1 Photo</li>
                                <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>800 caractères</li>
                                <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>Messagerie</li>

                                <div class="text-center"><a href="#" class="btn btn-outline-primary">Choisir</a></div>

                            </ul>

                        </div>
                    </div>
                    <!-- /pricing card -->
                </div>
                <div class="col-lg-4">
                    <!-- pricing card-->
                    <div class="card mb-5 mb-lg-0 border-0 card-highlight shadow-lg">
                        <div class="card-status bg-primary"></div>
                        <div class="card-body">
                            <h2 class="text-base subtitle text-center text-primary py-3">Argent</h2>
                            <p class="text-muted text-center"><span class="h1 text-dark">4€</span><span class="ml-2">/ mois</span></p>
                            <hr/>
                            <ul class="fa-ul my-4">
                                <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>Voir et repondre aux annonces</li>
                                <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>3 photos</li>
                                <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>1000 caractères</li>
                                <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>Messagerie</li>


                            </ul>
                            <div class="text-center"><a href="#" class="btn btn-outline-primary">Choisir</a></div>
                        </div>
                    </div>
                    <!-- /pricing card -->
                </div>
                <div class="col-lg-4">
                    <!-- pricing card-->
                    <div class="card mb-5 mb-lg-0 border-0 shadow">
                        <div class="card-body">
                            <h2 class="text-base subtitle text-center text-primary py-3">Or</h2>
                            <p class="text-muted text-center"><span class="h1 text-dark">5€</span><span class="ml-2">/ moi</span></p>
                            <hr/>
                            <ul class="fa-ul my-4">
                                <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>2 Annonces</li>

                                <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>Voir et repondre aux annonces</li>
                                <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>5 photos</li>
                                <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>2000 caractères</li>
                                <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>Messagerie</li>


                            </ul>
                            <div class="text-center"><a href="#" class="btn btn-outline-primary">Select</a></div>
                        </div>
                    </div>
                    <!-- /pricing card -->
                </div>
            </div>
        </div>
    </section>

@endsection
