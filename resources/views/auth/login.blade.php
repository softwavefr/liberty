@extends('layouts.app')
@section('content')
    <div class="container-fluid px-3">
        <div class="row min-vh-100">
            <div class="col-md-8 col-lg-6 col-xl-5 d-flex align-items-center">
                <div class="w-100 py-5 px-md-5 px-xl-6 position-relative">
                    <div class="mb-4">
                        <img src="img/logo-square.svg" alt="..." style="max-width: 4rem;" class="img-fluid mb-4">
                    </div>
                    <div class="close-absolute mr-md-5 mr-xl-6 pt-5">
                        <h2>Connexion</h2>
                    </div>
                    <form method="POST" action="{{ route('login') }}" class="form-validate">
                        @csrf
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right form-label">Adresse email</label>
                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="nom@address.com" name="email" value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right form-label">Mot de passe</label>
                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Mot de passe" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6 offset-md-6">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label class="form-check-label" for="remember">
                                        Se rappeler de moi
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary btn-block">Connexion</button>
                            </div>
                            <div class="col-md-12">
                                <br/>
                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        Mot de passe oublié ?
                                    </a>
                                @endif
                                <a class="btn btn-link" href="{{ route('register') }}">
                                    Pas encore de compte ?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4 col-lg-6 col-xl-7 d-none d-md-block">
                <div style="background-image: url(/img/photo/inscription.jpg);" class="bg-cover h-100 mr-n3"></div>
            </div>
        </div>
    </div>
@endsection
