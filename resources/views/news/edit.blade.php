@extends('layouts.app')
@section('content')

    <div class="container py-5">
        @guest
            <div class="alert alert-warning text-center alert-dismissible fade show" role="alert">
                Vous n'êtes pas connecté, vous ne pourrez pas modifier votre annonce ultérieurement<br>
                La création d'un compte est gratuite et vous offre de nombreuses fonctionnalités
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endguest
        <div class="shadow">
            <h2>Modification de votre article</h2>
            <div class="col-md-12">

                @if(session()->has('status'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('status') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                <form method="POST" action="{{ route('news.update', $news->id) }}">
                    @method('PUT')
                    @csrf
                    @include('partials.form-group', [
                       'title' => 'Titre',
                       'type' => 'text',
                       'name' => 'title',
                       'value' => $news->title,
                       'required' => true,
                   ])

                    <div class="form-group">
                        <label for="texte">Texte de votre article</label>
                        <textarea class="form-control{{ $errors->has('texte') ? ' is-invalid' : '' }} tinymce-editor" id="texte" name="texte" rows="10"
                                  maxlength="5000">{{ old('texte', isset($value) ? $value : $news->texte) }}</textarea>
                        @if ($errors->has('texte'))
                            <div class="invalid-feedback">
                                {{ $errors->first('texte') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="category">Catégorie</label>
                        {{ Form::select('category', $categories, null, array('class' => 'custom-select')) }}
                    </div>
                    <br>
                    <button type="submit" class="btn btn-primary">Valider</button>
                    <br/><br/>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')


    <script>
        Dropzone.options.myDropzone = {
            uploadMultiple: true,
            parallelUploads: 3,
            maxFilesize: 3,
            maxFiles: 3,
            dictMaxFilesExceeded: 'Vous ne pouvez charger que 3 photos',
            previewTemplate: document.querySelector('#preview').innerHTML,
            addRemoveLinks: true,
            acceptedFiles: 'image/*',
            dictInvalidFileType: 'Type de fichier interdit',
            dictRemoveFile: 'Supprimer',
            dictFileTooBig: 'L\'image fait plus de 3 Mo',
            timeout: 10000,
            init() {
                const myDropzone = this;
                $.get('{{ route('server-images') }}', data => {
                    $.each(data.images, (key, value) => {
                        const mockFile = {
                            name: value.original,
                            size: value.size,
                            dataURL: '{{ url('images') }}' + '/' + value.server
                        };
                        myDropzone.files.push(mockFile);
                        myDropzone.emit("addedfile", mockFile);
                        myDropzone.createThumbnailFromUrl(mockFile,
                            myDropzone.options.thumbnailWidth,
                            myDropzone.options.thumbnailHeight,
                            myDropzone.options.thumbnailMethod, true, (thumbnail) => {
                                myDropzone.emit('thumbnail', mockFile, thumbnail);
                            });
                        myDropzone.emit('complete', mockFile);
                    });
                });
                this.on("removedfile", file => {
                    $.ajax({
                        method: 'delete',
                        url: '{{ route('destroy-images') }}',
                        data: {name: file.name, _token: $('[name="_token"]').val()}
                    });
                });
            }
        };

    </script>

    <script src="https://cdn.tiny.cloud/1/8gw0lshon0bn94yzknjer4d2p46xizbxm2r9yp1v6rglyawn/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script type="text/javascript">
        tinymce.init({
            selector: 'textarea.tinymce-editor',
            //height: 100,
            menubar: false,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table paste code help wordcount'
            ],
            toolbar: 'undo redo | formatselect | ' +
                'bold italic backcolor | alignleft aligncenter ' +
                'alignright alignjustify | bullist numlist outdent indent | ' +
                'removeformat | help',
            toolbar_mode: 'floating'
        });
    </script>

@endsection
