@extends('layouts.app')
@section('content')

    <section class="py-6 bg-gray-100">
        <div class="container">
            <div class="row mb-5">
                <div class="col-md-8">
                    <p class="subtitle text-secondary">Les dernières informations </p>
                    <h2>Articles récents</h2>
                </div>
            </div>

            <div class="row">

                @foreach($news as $new)
                    <div class="col-md-6 col-sm-12 pt-6 mt-2">
                        @include('news.new', ['news' =>$new])
                    </div>
                @endforeach

            </div>

        </div>
    </section>

    <!-- Instagram-->
    <section>
        <div class="container-fluid px-0">
            <div class="swiper-container instagram-slider">
                <div class="swiper-wrapper">

                    <?php for ($i = 1; $i < 20; $i++) { ?>
                    <div class="swiper-slide overflow-hidden"><a href="#"><img src="img/instagram/<?php echo $i ?>.jpg" alt="" class="img-fluid hover-scale"></a></div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection
