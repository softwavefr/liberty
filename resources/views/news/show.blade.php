@extends('layouts.app')
@section('content')
    <div class="container py-5">
        <div id="massageOk" class="alert alert-success" role="alert" style="display: none">
            Votre message a été pris en compte et sera envoyé rapidement
        </div>
        @include('partials.message', ['url' => route('message')])

        @admin
            <a class="btn btn-warning btn-sm" href="{{ route('news.edit', $news->id) }}" role="button" data-toggle="tooltip" title="Modifier l'article"><i class="fas fa-edit"></i>Modifier l'article</a>
            <a class="btn btn-danger btn-sm" href="{{ route('news.destroy', $news->id) }}" role="button" data-toggle="tooltip" title="Supprimer l'article"><i class="fas fa-edit"></i>Supprimer l'article</a>
        @endadmin
        <div class="row">
            <div class="col-lg-12">
                <div class="text-block">

                    <h1>{{ $news->title }}</h1>

                    <p class="text-muted font-weight-light">
                        {!! $news->texte !!}
                    </p>
                </div>

            </div>
        </div>

        <div class="text-block">
            <div class="row gallery mb-3 ml-n1 mr-n1">
                @if($photos->isNotEmpty())
                    @if($photos->count() > 0)
                        @foreach ($photos as $photo)
                            <div class="col-lg-4 col-6 px-1 mb-2">
                                <a href="{{ asset('/images/' . $photo->filename) }}" data-fancybox="gallery" title="Outside">
                                    <img src="{{ asset('/images/' . $photo->filename) }}" alt="..." class="img-fluid">
                                </a>
                            </div>
                        @endforeach
                    @endif
                @endif
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script>
        $(() => {
            const toggleButtons = () => {
                $('#icon').toggle();
                $('#buttons').toggle();
            }
            $('#openModal').click(() => {
                $('#messageModal').modal();
            });
            $('#messageForm').submit((e) => {
                let that = e.currentTarget;
                e.preventDefault();
                $('#message').removeClass('is-invalid');
                $('.invalid-feedback').html('');
                toggleButtons();
                $.ajax({
                    method: $(that).attr('method'),
                    url: $(that).attr('action'),
                    data: $(that).serialize()
                })
                    .done((data) => {
                        toggleButtons();
                        $('#messageModal').modal('hide');
                        $('#massageOk').text(data.info).show();
                    })
                    .fail((data) => {
                        toggleButtons();
                        $.each(data.responseJSON.errors, function (i, error) {
                            $(document)
                                .find('[name="' + i + '"]')
                                .addClass('is-invalid')
                                .next()
                                .append(error[0]);
                        });
                    });
            });
        })
    </script>
@endsection
