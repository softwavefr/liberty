@extends('layouts.app')
@section('content')
    <div class="container py-5">
        <div class="jumbotron">
            <div class="row">
                <div class="col-md-4">
                    <img src="{{ asset('img/confirm.jpg') }}" alt="">
                </div>
                <div class="col-md-8">
                    <h1 class="display-4">Confirmation d'article</h1>
                    <p class="lead">Votre est validé</p>
                </div>
            </div>
        </div>
    </div>
@endsection
