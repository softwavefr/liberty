@extends('layouts.app')

@section('content')
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                @if (Session::has('message'))
                    <div class="alert alert-{{ Session::get('code') }}">
                        <p>{{ Session::get('message') }}</p>
                    </div>
                @endif

                <div class="panel panel-default">
                    <div class="panel-heading">Abonnement pour un mois</div>
                    <div class="panel-body">
                        <a href="{{ route('paypal.express-checkout') }}" class='btn-info btn'>15 €</a>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">Abonnement sur plusieurs mois</div>
                    <div class="panel-body">
                        <a href="{{ route('paypal.express-checkout', ['recurring' => true]) }}" class='btn-info btn'>10€ / mois</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
