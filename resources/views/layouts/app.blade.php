<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Les meilleures annonces</title>
    <!-- Styles -->
    <link rel="stylesheet" href="/vendor/nouislider/nouislider.css">
    <!-- Google fonts - Playfair Display-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700">
    <!-- Google fonts - Poppins-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,400i,700">
    <!-- swiper-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/css/swiper.min.css">
    <!-- Magnigic Popup-->
    <link rel="stylesheet" href="/vendor/magnific-popup/magnific-popup.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="/css/dropzone.css">
    <link rel="stylesheet" href="/css/card.css">

    <link rel="stylesheet" href="/css/app.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <!-- link href="{{ asset('css/app.css') }}" rel="stylesheet" -->

    <!-- Favicon-->
    <link rel="shortcut icon" href="img/favicon.png">
    @yield('css')

    <style>
        .chat {
            list-style: none;
            margin: 0;
            padding: 0;
        }

        .chat li {
            margin-bottom: 10px;
            padding-bottom: 5px;
            border-bottom: 1px dotted #B3A9A9;
        }

        .chat li .chat-body p {
            margin: 0;
            color: #777777;
        }

        .panel-body {
            overflow-y: scroll;
            /* height: 350px; */
            height: 92%;
        }

        ::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
            background-color: #F5F5F5;
        }

        ::-webkit-scrollbar {
            width: 12px;
            background-color: #F5F5F5;
        }

        ::-webkit-scrollbar-thumb {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
            background-color: #555;
        }
    </style>
</head>
<body>
<div>
    <nav class="navbar navbar-expand-lg fixed-top shadow navbar-light bg-white">
        <div class="container-fluid">
            <div class="d-flex align-items-center">
                <a href="{{ url('/') }}" class="navbar-brand py-1"><img src="/img/logo2.png" alt="logo world connexion"></a>
                <!--
                <form action="#" id="search" class="form-inline d-none d-sm-flex">
                    <div class="input-label-absolute input-label-absolute-left input-reset input-expand ml-lg-2 ml-xl-3">
                        <label for="search_search" class="label-absolute"><i class="fa fa-search"></i><span class="sr-only">What are you looking for?</span></label>
                        <input id="search_search" placeholder="Recherche " aria-label="Search" class="form-control form-control-sm border-0 shadow-0 bg-gray-200">
                        <button type="reset" class="btn btn-reset btn-sm"><i class="fa-times fas"></i></button>
                    </div>
                </form>
                -->
            </div>
            <button type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler navbar-toggler-right"><i class="fa fa-bars"></i></button>
            <!-- Navbar Collapse -->
            <div id="navbarCollapse" class="collapse navbar-collapse">
                <form action="#" id="searchcollapsed" class="form-inline mt-4 mb-2 d-sm-none">
                    <div class="input-label-absolute input-label-absolute-left input-reset w-100">
                        <label for="searchcollapsed_search" class="label-absolute"><i class="fa fa-search"></i><span class="sr-only">What are you looking for?</span></label>
                        <input id="searchcollapsed_search" placeholder="Search" aria-label="Search" class="form-control form-control-sm border-0 shadow-0 bg-gray-200">
                        <button type="reset" class="btn btn-reset btn-sm"><i class="fa-times fas">           </i></button>
                    </div>
                </form>

                <ul class="navbar-nav ml-auto">

                    <li class="nav-item @if(request()->route()->getName() == 'home') active @endif">
                        <a href="{{ url('/') }}" class="nav-link">{{ __('liberty.menu-home') }}</a>
                    </li>

                    <!-- Megamenu-->

                    <li class="nav-item @if(request()->route()->getName() == 'annonces.index') active @endif">
                        <a href="{{ url('annonces/') }}" class="nav-link">{{ __('liberty.menu-ad') }}</a>
                    </li>

                    <!-- /Megamenu end-->
                    <li class="nav-item @if(request()->route()->getName() == 'news.index') active @endif">
                        <a class="nav-link" href="{{ route('news.index') }}">{{ __('liberty.menu-item') }}</a>
                    </li>

                    <li class="nav-item @if(request()->route()->getName() == 'group') active @endif">
                        <a class="nav-link" href="{{ route('group') }}">{{ __('liberty.menu-community') }}</a>
                    </li>
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item @if(request()->route()->getName() == 'register') active @endif">
                            <a class="nav-link" href="{{ url('enregistrement') }}">{{ __('liberty.menu-registration') }}</a>
                        </li>
                        <li class="nav-item @if(request()->route()->getName() == 'login') active @endif">
                            <a class="btn btn-primary" href="{{ route('login') }}">{{ __('liberty.menu-login') }}</a>
                        </li>
                    @else
                        @admin
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('admin.index') }}">{{ __('liberty.menu-admin') }}</a>
                            </li>
                        @else

                            <li class="nav-item @if(request()->route()->getName() == 'annonces.create') active @endif">
                                @if (isset(auth()->user()->subscription))
                                <a class="nav-link" href="{{ route('annonces.create') }}" role="button">{{ __('liberty.menu-new-ad') }}</a>
                                @else
                                    <a class="nav-link disabled" role="button">{{ __('liberty.menu-new-ad') }}</a>
                                @endif
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('user.index') }}">{{ __('liberty.menu-account') }}</a>
                            </li>
                            <li class="nav-item @if(request()->route()->getName() == 'chat.liste') active @endif">
                                @if (isset(auth()->user()->subscription))
                                <a class="nav-link" href="{{ url('/chat') }}">{{ __('liberty.menu-message') }}</a>
                                @else
                                <a class="nav-link disabled" href="{{ url('/chat') }}">{{ __('liberty.menu-message') }}</a>
                                @endif
                            </li>
                        @endadmin
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('logout') }}"
                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    {{ __('liberty.menu-disconnection') }}
                                </a>
                                <small id="745a54s">
                                    {{ auth()->user()->name }} - {{ auth()->user()->id }} - {{ auth()->user()->subscription }}
                                </small>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        @endguest

                        <li class="nav-link">
                            <a href="{{ url('locale/en') }}"><img src="/img/country/united-kingdom-flag.png"></a>
                            <a href="{{ url('locale/fr') }}"><img src="/img/country/france-flag.png"></a>
                            <a href="{{ url('locale/es') }}"><img src="/img/country/spain-flag.png"></a>
                        </li>

                </ul>
            </div>
        </div>
    </nav>

    <main class="pt-6">
        @yield('content')
    </main>

    <!-- Footer-->
    <nav>
    <footer class="position-relative z-index-10 d-print-none">
        <!-- Main block - menus, subscribe form-->
        <div class="py-6 bg-gray-200 text-muted">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 mb-5 mb-lg-0">
                        <div class="font-weight-bold text-uppercase text-dark mb-3">World'Connexion</div>
                        <img src="/img/logo.png" alt="logo world connexion" >

                        <!--
                        <p>LA MEILLEURE EXPÉRIENCE D'ANNONCE LIBERTINE DE LA CARAÎBE</p>
                        <ul class="list-inline">
                            <li class="list-inline-item"><a href="#" target="_blank" title="twitter" class="text-muted text-hover-primary"><i class="fab fa-twitter"></i></a></li>
                            <li class="list-inline-item"><a href="#" target="_blank" title="facebook" class="text-muted text-hover-primary"><i class="fab fa-facebook"></i></a></li>
                            <li class="list-inline-item"><a href="#" target="_blank" title="instagram" class="text-muted text-hover-primary"><i class="fab fa-instagram"></i></a></li>
                            <li class="list-inline-item"><a href="#" target="_blank" title="pinterest" class="text-muted text-hover-primary"><i class="fab fa-pinterest"></i></a></li>
                            <li class="list-inline-item"><a href="#" target="_blank" title="vimeo" class="text-muted text-hover-primary"><i class="fab fa-vimeo"></i></a></li>
                        </ul>
                        -->
                    </div>

                    <div class="col-lg-2 col-md-6 mb-5 mb-lg-0">
                        <h6 class="text-uppercase text-dark mb-3">Pages</h6>
                        <ul class="list-unstyled">
                            <li><a href="" class="text-muted">Accueil                                  </a></li>
                            <li><a href="pricing.html" class="text-muted">Annonce                                  </a></li>
                            <li><a href="text.html" class="text-muted">Actualité                                   </a></li>
                            <li><a href="faq.html" class="text-muted">Inscription                                   </a></li>
                            <li><a href="coming-soon.html" class="text-muted">Connexion                                  </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-4 col-md-6 mb-5 mb-lg-0">
                        <h6 class="text-uppercase text-dark mb-3">Administratif</h6>
                        <ul class="list-unstyled">
                            <li>
                                <a class="text-muted" href="{{ route('legal') }}">Mentions légales</a>
                            </li>
                            <li>
                                <a class="text-muted" href="{{ route('confidentialite') }}">Politique de confidentialité</a>
                            </li>
                            <li>
                                <a class="text-muted" href="{{ route('information') }}">Condition d'utilisation</a>
                            </li>

                        </ul>
                    </div>
                    <!--
                    <div class="col-lg-4">
                        <h6 class="text-uppercase text-dark mb-3">Inscrivez vous à notre newsletter</h6>
                        <p class="mb-3"> Vous  recevez  tout les bons plan </p>
                        <form action="#" id="newsletter-form">
                            <div class="input-group mb-3">
                                <input type="email" placeholder="Votre mail" aria-label="Your Email Address" class="form-control bg-transparent border-dark border-right-0">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-outline-dark border-left-0"> <i class="fa fa-paper-plane text-lg"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                    -->
                </div>
            </div>
        </div>
        <!-- Copyright section of the footer-->
        <div class="py-4 font-weight-light bg-gray-800 text-gray-300">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-6 text-center text-md-left">
                        <p class="text-sm mb-md-0">&copy; 2019 Libert'iles.  All rights reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    </nav>
    <!-- /Footer end-->
</div>

<script src="/js/dropzone.js"></script>
<script>
    // ------------------------------------------------------- //
    //   Inject SVG Sprite -
    //   see more here
    //   https://css-tricks.com/ajaxing-svg-sprite/
    // ------------------------------------------------------ //
    function injectSvgSprite(path) {

        var ajax = new XMLHttpRequest();
        ajax.open("GET", path, true);
        ajax.send();
        ajax.onload = function(e) {
            var div = document.createElement("div");
            div.className = 'd-none';
            div.innerHTML = ajax.responseText;
            document.body.insertBefore(div, document.body.childNodes[0]);
        }
    }
    // to avoid CORS issues when viewing using file:// protocol, using the demo URL for the SVG sprite
    // use your own URL in production, please :)
    // https://demo.bootstrapious.com/directory/1-0/icons/orion-svg-sprite.svg
    //- injectSvgSprite('icons/orion-svg-sprite.svg');
    injectSvgSprite('https://demo.bootstrapious.com/directory/1-1/icons/orion-svg-sprite.svg');

</script>
<!-- jQuery-->
<script src="/vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap JS bundle - Bootstrap + PopperJS-->
<script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Magnific Popup - Lightbox for the gallery-->
<script src="/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
<!-- Smooth scroll-->
<script src="/vendor/smooth-scroll/smooth-scroll.polyfills.min.js"></script>
<!-- Bootstrap Select-->
<script src="/vendor/bootstrap-select/js/bootstrap-select.min.js"></script>
<!-- Object Fit Images - Fallback for browsers that don't support object-fit-->
<script src="/vendor/object-fit-images/ofi.min.js"></script>
<!-- Swiper Carousel                       -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/js/swiper.min.js"></script>
<script>var basePath = ''</script>
<!-- Main Theme JS file    -->
<script src="/js/theme.js"></script>
@yield('script')
</body>
</html>
