@extends('layouts.user')
@section('content')

    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Abonnement</h1>
    </div>
    <div class="row">



        <?php

        /*
        $user = auth()->user();

        echo $user->stripe_customer;


        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET_KEY'));

        $customer = \Stripe\Customer::retrieve($user->stripe_customer);
        $cardID = $customer->default_source;

        if(isset($cardID)){
            echo "<br/>CARTE ".$cardID;
        } else {
            echo "No card";
            //Code for entering card info..
        }

        $customer = \Stripe\Customer::retrieve($user->stripe_customer);
        $abonnements = $customer->subscriptions;

        foreach ($abonnements as $abo)
        {
            echo "<br/>".$abo->plan->id;
            echo "<br/>".$abo->plan->nickname;
            echo "<br/>".$abo->status;

            var_dump($abo);
        }



    echo "<hr/>";
        $response = \Stripe\Customer::all(["limit" => 100, "email" => $user->email]);
      //  var_dump($response->data);

*/
        ?>

    </div>

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Tableau de bord</h1>
    </div>
    <!-- Content Row -->
    <div class="row">
        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Annonces actives</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $adActivesCount }}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-hiking fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Annonces en attente</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $adAttenteCount }}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-hourglass-start fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-danger shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Annonces obsolètes</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $adPerimesCount }}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-hourglass-end fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
