<?php

return [


    'info' => 'Nos informations',
    'section' => 'Nos articles',
    'news' => 'A la une',
    'coverage' => 'Reportage',
    'feed' => 'Votre fil d\'actualité',
    'ads' => 'Votre publicité ici',
    'tips' => 'Les derniers bons plans',
    'announce' => 'Annonces récentes',
    'listing' => 'Voir toutes les annonces',

    'menu-home' => 'Accueil',
    'menu-ad' => 'Annonce',
    'menu-item' => 'Article',
    'menu-community' => 'Communauté',
    'menu-new-ad' => 'Nouvelle annonce',
    'menu-account' => 'Mon compte',
    'menu-message' => 'Message',
    'menu-disconnection' => 'Déconnexion',
    'menu-registration' => 'Inscription',
    'menu-login' => 'Connexion',
    'menu-admin' => 'Administration',

    'CAT_1' => 'Rencontre',
    'CAT_2' => 'Escort girl',
    'CAT_3' => 'Massage',
    'CAT_4' => 'Soirée libertine',
    'CAT_5' => 'Senior',
    'CAT_6' => 'Amical',
    'CAT_7' => 'Sérieuse',
    'CAT_8' => 'Rencontre X',
    'CAT_9' => 'Rencontre gay',
    'CAT_10' => 'Rencontre lesbiennes',
    'CAT_11' => 'Transsexuels',
    'CAT_12' => 'Libertin',


];
