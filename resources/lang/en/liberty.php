<?php

return [

    'connect-world' => 'your connected world',
    'info' => 'Our information',
    'section' => 'Our articles',
    'news' => 'In the news',
    'coverage' => 'Coverage',
    'feed' => 'Your news feed',
    'ads' => 'Your advertisement here',
    'tips' => 'The latest tips',
    'announce' => 'Recent announcements',
    'listing' => 'View all listings',

    'menu-home' => 'Home',
    'menu-ad' => 'Ad',
    'menu-item' => 'Item',
    'menu-community' => 'Community',
    'menu-new-ad' => 'New ad',
    'menu-account' => 'My account',
    'menu-message' => 'Message',
    'menu-disconnection' => 'Disconnection',
    'menu-registration' => 'Registration',
    'menu-login' => 'Login',
    'menu-admin' => 'Administration',

    'CAT_1' => 'Meeting',
    'CAT_2' => 'Escort girl',
    'CAT_3' => 'Massage',
    'CAT_4' => 'Libertine evening',
    'CAT_5' => 'Senior',
    'CAT_6' => 'Friendly',
    'CAT_7' => 'Serious',
    'CAT_8' => 'Meet X',
    'CAT_9' => 'Gay dating',
    'CAT_10' => 'Lesbian dating',
    'CAT_11' => 'Transsexuals',
    'CAT_12' => 'Libertine',

];

// {{ __('liberty.menu-') }}
